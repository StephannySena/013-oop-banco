"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Cliente = void 0;
var Cliente = /** @class */ (function () {
    function Cliente(nome, cpf, rg, endereco, conta) {
        this.nome = nome;
        this.cpf = cpf;
        this.rg = rg;
        this.endereco = endereco;
        this.conta = conta;
    }
    Object.defineProperty(Cliente.prototype, "nome", {
        //GETTERS
        get: function () {
            return this._nome;
        },
        //SETTERS
        set: function (nome) {
            this._nome = nome;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Cliente.prototype, "cpf", {
        get: function () {
            return this._cpf;
        },
        set: function (cpf) {
            if (this.validaCpf(cpf)) {
                this._cpf = cpf;
            }
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Cliente.prototype, "rg", {
        get: function () {
            return this._rg;
        },
        set: function (rg) {
            this._rg = rg;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Cliente.prototype, "endereco", {
        get: function () {
            return this._endereco;
        },
        set: function (endereco) {
            this._endereco = endereco;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Cliente.prototype, "conta", {
        get: function () {
            return this._conta;
        },
        set: function (conta) {
            this._conta = conta;
        },
        enumerable: false,
        configurable: true
    });
    Cliente.prototype.validaCpf = function (cpf) {
        if (cpf != "") {
            return true;
        }
        else {
            return false;
        }
    };
    return Cliente;
}());
exports.Cliente = Cliente;
