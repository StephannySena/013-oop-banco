import { Conta } from "./conta";

export class Cliente {
    private _nome: string;
    private _cpf: string;
    private _rg: string;
    private _endereco: string;
    private _conta: Conta;


    constructor(nome: string, cpf: string, rg: string, endereco: string, conta: Conta){
        this.nome = nome;
        this.cpf = cpf;
        this.rg = rg;
        this.endereco = endereco;
        this.conta = conta;
    }

    //GETTERS
    public get nome(): string {
        return this._nome;
    }

    public get cpf(): string {
        return this._cpf;
    }

    public get rg(): string {
        return this._rg;
    }

    public get endereco(): string {
        return this._endereco;
    }

    public get conta(): Conta {
        return this._conta;
    }

    //SETTERS
    public set nome(nome: string) {            
        this._nome = nome;
        
    }

    public set cpf(cpf: string) {
        if (this.validaCpf(cpf)){
        this._cpf =  cpf;
        }
    }

    public set rg(rg: string) {
        this._rg = rg;
    }

    public set endereco(endereco: string) {
        this._endereco = endereco;
    }

    public set conta(conta: Conta) {
        this._conta = conta;
    }


    private validaCpf(cpf: string) {
        if (cpf != "") {
            return true
        }
        else {
            return false
        }
    }
}