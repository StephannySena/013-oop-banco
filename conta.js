"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Conta = void 0;
var Conta = /** @class */ (function () {
    function Conta(numConta, saldo, limite, tipo, senha) {
        this.numConta = numConta;
        this.saldo = saldo;
        this.limite = limite;
        this.tipo = tipo;
        this.senha = senha;
    }
    Object.defineProperty(Conta.prototype, "numConta", {
        //GETTERS
        get: function () {
            return this._numConta;
        },
        //SETTERS
        set: function (numConta) {
            this._numConta = numConta;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Conta.prototype, "saldo", {
        get: function () {
            return this._saldo;
        },
        set: function (saldo) {
            this._saldo = saldo;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Conta.prototype, "limite", {
        get: function () {
            return this._limite;
        },
        set: function (limite) {
            this._limite = limite;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Conta.prototype, "tipo", {
        get: function () {
            return this._tipo;
        },
        set: function (tipo) {
            this._tipo = tipo;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Conta.prototype, "senha", {
        get: function () {
            return this._senha;
        },
        set: function (senha) {
            this._senha = senha;
        },
        enumerable: false,
        configurable: true
    });
    //MÉTODOS
    Conta.prototype.depositar = function (valor) {
        if (valor > 0) {
            this.saldo = this.saldo + valor;
            return this.saldo;
        }
        else {
            return "Valor de depósito inválido.";
        }
    };
    Conta.prototype.sacar = function (valor) {
        var totalDisponivel = this.saldo + this.limite;
        if (valor <= 0) {
            return "Insira um valor válido";
        }
        else if (valor <= this.saldo) {
            this.saldo = this.saldo - valor;
            return this.saldo;
        }
        else if (valor <= totalDisponivel) {
            var debLimite = valor - this.saldo;
            this.saldo = 0;
            this.limite = this.limite - debLimite;
            return this.limite, this.saldo;
        }
        else {
            return "Valor de saque excede o total disponível";
        }
    };
    return Conta;
}());
exports.Conta = Conta;
