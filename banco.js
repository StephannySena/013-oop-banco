"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Banco = void 0;
var Banco = /** @class */ (function () {
    function Banco(nome, cnpj, agencia, clientes) {
        this.nome = nome;
        this.cnpj = cnpj;
        this.agencia = agencia;
        this.clientes = clientes;
    }
    Object.defineProperty(Banco.prototype, "nome", {
        //GETTERS
        get: function () {
            return this._nome;
        },
        //SETTERS
        set: function (nome) {
            this._nome = nome;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Banco.prototype, "cnpj", {
        get: function () {
            return this._cnpj;
        },
        set: function (cnpj) {
            this._cnpj = cnpj;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Banco.prototype, "agencia", {
        get: function () {
            return this._agencia;
        },
        set: function (agencia) {
            this._agencia = agencia;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Banco.prototype, "clientes", {
        get: function () {
            return this._clientes;
        },
        set: function (clientes) {
            this._clientes = clientes;
        },
        enumerable: false,
        configurable: true
    });
    Banco.prototype.adicionaCliente = function (cliente) {
        this.clientes.push(cliente);
        return this.clientes;
    };
    Banco.prototype.getCliente = function (value) {
        return this.clientes[value];
    };
    return Banco;
}());
exports.Banco = Banco;
