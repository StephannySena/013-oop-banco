import {Cliente} from "./cliente";

export class Banco{
    private _nome: string;
    private _cnpj: string;
    private _agencia: number;
    private _clientes: Array<Cliente>;



    constructor(nome: string, cnpj: string, agencia: number, clientes: Array<Cliente>){
        this.nome = nome;
        this.cnpj = cnpj;
        this.agencia = agencia;
        this.clientes = clientes;
    }

    //GETTERS
    public get nome(): string {
        return this._nome;
    }
    public get cnpj(): string {
        return this._cnpj;
    }
    public get agencia(): number {
        return this._agencia;
    }
    public get clientes(): Array<Cliente> {
        return this._clientes;
    }
    //SETTERS
    public set nome(nome: string) {
        this._nome = nome;
    }
    public set cnpj(cnpj: string) {
        this._cnpj = cnpj;
    }
    public set agencia(agencia: number) {
        this._agencia = agencia;
    }
    public set clientes(clientes: Array<Cliente>) {
        this._clientes = clientes;
    }


    public adicionaCliente(cliente: Cliente){
        this.clientes.push(cliente);

        return this.clientes
    }

    public getCliente(value: number){
        return this.clientes[value];
    }
}