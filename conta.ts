export class Conta {
    private _numConta: number;
    private _saldo: number;
    private _limite: number;
    private _tipo: number;
    private _senha: number;

    constructor(numConta: number, saldo: number, limite: number, tipo: number, senha: number) {
        this.numConta = numConta;
        this.saldo = saldo;
        this.limite = limite;
        this.tipo = tipo;
        this.senha = senha;
    }

    //GETTERS
    public get numConta(): number {
        return this._numConta;
    }

    public get saldo(): number {
        return this._saldo;
    }

    public get limite(): number {
        return this._limite;
    }

    public get tipo(): number {
        return this._tipo;
    }

    public get senha(): number {
        return this._senha;
    }

    //SETTERS
    public set numConta(numConta: number) {
        this._numConta = numConta;
    }

    public set saldo(saldo: number) {
        this._saldo = saldo;
    }

    public set limite(limite: number) {
        this._limite = limite;
    }

    public set tipo(tipo: number) {
        this._tipo = tipo;
    }

    public set senha(senha: number) {
        this._senha = senha;
    }

    //MÉTODOS

    public depositar(valor: number) {
        if (valor > 0) {
            this.saldo = this.saldo + valor;
            return this.saldo;
        }
        else {
            return "Valor de depósito inválido."
        }



    }

    public sacar(valor: number) {
        var totalDisponivel = this.saldo + this.limite

        if(valor <= 0){
            return "Insira um valor válido"
        }
        else if (valor <= this.saldo) {
            this.saldo = this.saldo - valor;

            return this.saldo

        }
        else if (valor <= totalDisponivel) {
            var debLimite = valor - this.saldo;

            this.saldo = 0;
            this.limite = this.limite - debLimite;
            return this.limite, this.saldo

        }
        else {

            return "Valor de saque excede o total disponível"
        }
    }
}